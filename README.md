# Documentation for the DS-Proto V1725 DAQ

Please see the documentation here:

https://bitbucket.org/ttriumfdaq/dsproto_daq/wiki/Home

The custom pages are designed to work for a frontend index of 0. Run like `./feov1725 -i 0`.

