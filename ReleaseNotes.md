# DS-Proto DAQ Release Notes

## v1.2.1

### New features

* Improve chronobox webpage to have fewer hard-coded parameters.

### Bugfixes

N/A


## v1.2.0

### New features

* Add setup script for setting up CDM to use external clock source.
* Make the chronobox IP address configurable in the ODB.

### Bugfixes

N/A


## v1.1.0

### New features

* Update URLs on Chronobox helper page (run_type.html) to match Napoli setup.
* Remove V1725 self-trigger threshold from Chronobox helper page (run_type.html) as these are best handled by save/load webpage.

### Bugfixes

N/A


## v1.0.1

### New features

* Add dynamic range and trigger couple logic options to custom webpage.

### Bugfixes

N/A


## v1.0.0

Initial release using git-flow.

Supports DAQ setup at Napoli, including running with a CDM bit without a Chronobox.